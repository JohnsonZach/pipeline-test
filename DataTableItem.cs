﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp
{
    public class DataTableItem
    {
        public DataTableItem(string name, string uom, string number1, string number2, string number3)
        {
            Name = name;
            UoM = uom;
            Number1 = number1;
            Number2 = number2;
            Number3 = number3;
        }

        public string Name { get; set; }
        public string UoM { get; set; }
        public string Number1 { get; set; }
        public string Number2 { get; set; }
        public string Number3 { get; set; }
    }
}
