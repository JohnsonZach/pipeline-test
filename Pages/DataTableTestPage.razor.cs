using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BlazorApp.Pages
{
    public partial class DataTableTestPage
    {
        List<DataTableItem> dataItems = new List<DataTableItem>();
        public string paragraphCss;
        public string headerCss;
        public string table1Css;
        public string table1HeaderCss;
        public string table1RowCss;
        public string table2Css;
        public string table2HeaderCss;
        public string table2RowCss;
        public int rowCount;
        public string _background;

        protected override void OnInitialized()
        {
            rowCount = 0;
            dataItems.Add(new DataTableItem("Order Number", "", "", "", ""));
            dataItems.Add(new DataTableItem("RAN Files Directory", "", "\\srv-Ibu-dataEngineering", "C:CCDEiCTVTestUnits", "C:CCDEIEiCTVTestUnits"));
            dataItems.Add(new DataTableItem("Production Site", "", "La Crosse", "La Crosse", "La Crosse"));
        }
        
        public void setRowBackgroundGray()
        {
            _background = "#ABABAB";
        }

        public void setRowBackgroundWhite()
        {
            _background = "#FFFFFF";
        }
    }
}