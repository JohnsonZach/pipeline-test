﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;

namespace BlazorApp.Pages
{
    public partial class PostTesting : ComponentBase //This is a code-behind for PostTesting for readability
    {
        [Inject]
        AuthenticationStateProvider AuthStateProvider { get; set; }


        private PostTestingEditFormModel _formModel { get; set; }
        private AuthenticationState _myAuth { get; set; }

        protected override void OnInitialized()
        {
            _formModel = new PostTestingEditFormModel();
            _formModel.Word = "Word";

            

            base.OnInitialized();
        }

        protected override Task OnInitializedAsync()
        {
            _myAuth = AuthStateProvider.GetAuthenticationStateAsync().Result;
            return base.OnInitializedAsync();
        }
    }

    public class PostTestingEditFormModel
    { 
        [Required]
        public string Word { get; set; }

        
    }
}
