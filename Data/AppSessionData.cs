﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Data
{
    public class AppSessionData
    {
        public String SessionId { get; private set; }
        public String SessionRemoteIp { get; private set; }

        public AppSessionData(String sessionId, String xFwdFor = null)
        {
            SessionId = sessionId;
            SessionRemoteIp = (xFwdFor != null) ? xFwdFor.Split(',').FirstOrDefault() : "NULL";
            Console.WriteLine($"[DEBUG] AppSessionData constructed successfully: {SessionId}, {SessionRemoteIp}");
        }
    }
}
