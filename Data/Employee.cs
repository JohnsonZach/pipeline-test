using System;

namespace BlazorApp.Data
{
    public class Employee
    {
        public float Salary { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

    }
}
