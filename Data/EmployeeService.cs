using System;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Data
{
    public class EmployeeService
    {
        private static readonly string[] FirstNames = new[]
        {
            "James", "John", "Robert", "Michael", "Oliver", "Ava", "William", "Sophia", "Ashley", "Elijah"
        };

        private static readonly string[] LastNames = new[]
{
            "Smith", "Johnson", "Brown", "Miller", "Jones", "Anderson", "Taylor", "Lee", "Harris", "Walker"
        };

        public static Task<Employee[]> GetEmployesAsync()
        {
            var rng = new Random();
            return Task.FromResult(Enumerable.Range(1, 10).Select(index => new Employee
            {
                FirstName = FirstNames[rng.Next(FirstNames.Length)],
                LastName = LastNames[rng.Next(LastNames.Length)],
                Salary = rng.Next(10000)
            }).ToArray());
        }
    }
}
